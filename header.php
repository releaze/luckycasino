<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package luckycasino
 */

  $post_id   = get_the_ID();
  $team_1    = get_post_meta($post_id, 'team_1', true);
  $team_2    = get_post_meta($post_id, 'team_2', true);

  $evergreen_post = get_posts(
     array(
          'posts_per_page' => -1,
       // 'tag'            => 'ab-evergreen-teams',
          'post_status'    => array( 'publish' ),
          'post_type'      => 'post',
          'orderby'        => 'date',
          'order'          => 'DESC',
          'meta_query' => array(
                   array(
                       'key'     => 'team_1',
                       'value'   => array($homeID, $awayID),
                       'compare' => 'IN',
                   ),
                   array(
                       'key'     => 'team_2',
                       'value'   => array($homeID, $awayID),
                       'compare' => 'IN',
                   )
               )
           )
    );

 $ev_post    = $evergreen_post[0];
 $post_name  = $ev_post->post_name;
 $url        = get_site_url();

  ob_start();
  wp_loginout('index.php');
  $loginoutlink = ob_get_contents();
  ob_end_clean();
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<?php
        if (!empty($team_1) || !empty($team_2) || $evergreen_post) {
            ?>
                <!-- Canonical URL by Evergreen Post -->
                <link rel="canonical" href=<?php echo $url  . "/" . $post_name ?>>
            <?php
        }
    ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'luckycasino' ); ?></a>

	<header id="masthead" class="site-header">
            <div class="site-branding-wrapper">
                   <div></div>
                   <div class="site-branding">
                       <?php the_custom_logo(); ?>
                   </div>
                   <button class="primary-button"><a href="https://luckycasino.com/sv/sports/">Spela här</a></button>
            </div>

           <div class="header-wrapper">
               <div class="site-branding">
                   <?php
                   if ( is_front_page() && is_home() ) :
                       ?>
                       <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                       <?php
                   else :
                       ?>
                       <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                       <?php
                   endif;
                   $luckycasino_description = get_bloginfo( 'description', 'display' );
                   if ( $luckycasino_description || is_customize_preview() ) :
                       ?>
                       <p class="site-description"><?php echo $luckycasino_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
                   <?php endif; ?>

                   <button class="primary-button play-here-anim"><a href="https://luckycasino.com/sv/sports/">Spela här</a></button>
               </div>
           </div>
	</header><!-- #masthead -->

	<div class="menu-wrapper sticky">
       <div class="container flex">
           <nav id="site-navigation" class="main-navigation">
               <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                 <span><em></em></span>
               </button>

               <div id="general-menu">
                   <div class="mobile-tabs">
                       <ul>
                        <li class="active">Betting</li>
                        <?php if ( has_nav_menu( 'tournament-menu' ) ){ ?>
                          <li>Euro2020</li>
                        <?php } ?>
                       </ul>
                   </div>

                   <div id="menus">
                        <?php wp_nav_menu(
                           array(
                               'theme_location' => 'menu-1',
                                'menu_id'        => 'primary-menu',
                                'menu_class'     => 'betting show',
                            ));
                        ?>

                        <?php
                            if ( has_nav_menu( 'tournament-menu' ) ){
                                wp_nav_menu(
                                   array(
                                       'theme_location' => 'tournament-menu',
                                       'menu_class'     => 'mobile-tournaments-menu euro2020',
                                    )
                                );
                            }
                        ?>
                   </div>

                   <script>
                       const links = document.getElementById("menus");
                       const toggleMenu = document.querySelector('.mobile-tabs ul');
                       const items = toggleMenu.getElementsByTagName("li");

                       for (let i = 0; i < items.length; i++) {
                           items[i].addEventListener("click", function(event) {
                               if (event.target.tagName === 'LI') {
                                   const current = document.querySelector(".active");
                                   current.classList.remove('active');
                                   this.classList.add('active');

                                   const activeMenuName = items[i].innerText.toLowerCase();
                                   const prevLinksMenu = document.querySelector(".show");
                                   prevLinksMenu.classList.remove('show');

                                   const activeLinksMenu = document.querySelector(`.${activeMenuName}`);
                                   activeLinksMenu.classList.add('show');
                               }
                           });
                       }
                   </script>
               </div>
           </nav>

            <?php if ( has_nav_menu( 'tournament-menu' ) ){ ?>
              <div class="tournament-menu">
                <span class="euro-icon"></span>
            <?php
                wp_nav_menu(
                  array(
                     'theme_location' => 'tournament-menu',
                     'menu_class'     => 'menu',
                  )
               );
            ?>

            </div>
            <?php } ?>

            <div class="site-branding single-post">
               <?php the_custom_logo(); ?>
           </div>

           <button class="primary-button single-post"><a href="https://luckycasino.com/sv/sports/">Spela här</a></button>
       </div>
   </div>
