<?php
/*
Template Name: Live Page Template
Template Post Type: page
*/

get_header();
?>
    <div class="container">
            <div class='top-sidebar'>
                <div class="widget-area">
                    <?php dynamic_sidebar( 'top-sidebar' ); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 col-xl-8">
                    <div class="wrapper">
                        <main id="primary" class="site-main">
                            <?php
                                while ( have_posts() ) :
                                    the_post();

                                    get_template_part( 'template-parts/content', 'page' );

                                    // If comments are open or we have at least one comment, load up the comment template.
                                    if ( comments_open() || get_comments_number() ) :
                                        comments_template();
                                    endif;

                                endwhile; // End of the loop.
                            ?>
                        </main><!-- #main -->
                    </div>
                </div>
                <div class="col-lg-4 col-xl-4">
                    <div class="wrapper wrapper__widget-area">
                        <aside class="widget-area">
                            <?php dynamic_sidebar( 'live-page-sidebar' ); ?>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
<?php
get_footer();
