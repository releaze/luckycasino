<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package luckycasino
 */

get_header();
$thumbnail_url = get_the_post_thumbnail_url( get_the_ID(), 'fullwidth-post-thumb' );
?>
<div class="fullwidth-post-thumbnail" style=<?php  echo "background-image:url(" . $thumbnail_url . ");" ?>>
    <div class="container">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="title-link"><?php the_title(); ?></a>

        <?php
            $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
            if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
                $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
            }
            $time_string = sprintf(
                $time_string,
                esc_attr( get_the_date( DATE_W3C ) ),
                esc_html( get_the_date() ),
                esc_attr( get_the_modified_date( DATE_W3C ) ),
                esc_html( get_the_modified_date() )
            );

            $posted_on = sprintf(
                /* translators: %s: post date. */
                esc_html_x( 'Postat %s', 'post date', 'luckycasino' ),
                '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
            );

            echo '<span class="posted-on">' . $posted_on . '</span>';
        ?>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-xl-8">
            <div class="wrapper">
                <main id="primary" class="site-main">
                       <div class="si-default-post-layout">
                            <?php
                            while ( have_posts() ) :
                                the_post();

                                get_template_part( 'template-parts/content', get_post_type() );

                                the_post_navigation(
                                    array(
                                        'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'luckycasino' ) . '</span> <span class="nav-title">%title</span>',
                                        'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'luckycasino' ) . '</span> <span class="nav-title">%title</span>',
                                    )
                                );

                            endwhile; // End of the loop.
                            ?>
                        </div>
                	</main><!-- #main -->
            </div>
        </div>
        <div class="col-lg-4 col-xl-4">
            <div class="wrapper wrapper__widget-area">
                <aside class="widget-area">
                    <?php get_sidebar(); ?>
                </aside>
            </div>
        </div>
    </div>
</div>
<?php
get_sidebar();
get_footer();
