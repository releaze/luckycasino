<?php
/**
 * luckycasino functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package luckycasino
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'luckycasino_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function luckycasino_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on luckycasino, use a find and replace
		 * to change 'luckycasino' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'luckycasino', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'luckycasino' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'luckycasino_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'luckycasino_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function luckycasino_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'luckycasino_content_width', 640 );
}
add_action( 'after_setup_theme', 'luckycasino_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function luckycasino_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'luckycasino' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'luckycasino' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'luckycasino_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function luckycasino_scripts() {
	wp_enqueue_style( 'luckycasino-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'luckycasino-style', 'rtl', 'replace' );

	wp_enqueue_script( 'luckycasino-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'luckycasino_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//======================================================================================================================
add_action( 'wp_enqueue_scripts', 'luckycasino_style_theme' );
add_action( 'widgets_init', 'luckycasino_register_sidebar' );
add_action( 'init', 'luckycasino_custom_menus' );
add_action( 'after_setup_theme', 'luckycasino_fullwidth_latest_image_size' );
add_action( 'after_setup_theme', 'luckycasino_latest_image_size' );
add_action( 'after_setup_theme', 'luckycasino_fullwidth_post_thumbnail_image_size' );

add_shortcode( 'fullwidth_latest_post', 'luckycasino_fullwidth_latest_post_shortcode_function' );
add_shortcode( 'latest_posts', 'luckycasino_latest_posts_shortcode_function' );

// Adds styles
function luckycasino_style_theme(){
    wp_enqueue_style( 'bootstrap_grid', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap-grid.min.css');
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}

//Custom menu
function luckycasino_custom_menus() {
  register_nav_menus(
    array(
      'tournament-menu' => __( 'Tournament Menu' ),
    )
  );
}

//Custom sidebars
function luckycasino_register_sidebar(){
	register_sidebar( array(
		'name' => "Top Sidebar",
		'id' => 'top-sidebar',
		'description' => 'These widgets will be shown under the Header',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	) );
	register_sidebar( array(
        'name' => "Live Page Sidebar",
        'id' => 'live-page-sidebar',
        'description' => 'These widgets will be shown in Live Page Sidebar',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );
    register_sidebar( array(
        'name' => "Groups Page Sidebar",
        'id' => 'groups-page-sidebar',
        'description' => 'These widgets will be shown in Groups Page Sidebar',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );
    register_sidebar( array(
        'name' => "Footer Sidebar",
        'id' => 'footer-sidebar',
        'description' => 'These widgets will be shown in Footer',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );
}

//Images
function luckycasino_fullwidth_latest_image_size(){
    add_image_size( 'fullwidth-latest-post-thumb', 764, 367, true );
}

function luckycasino_latest_image_size(){
    add_image_size( 'latest-post-thumb', 238, 156, true );
}

function luckycasino_fullwidth_post_thumbnail_image_size(){
    add_image_size( 'fullwidth-post-thumb', 1440, 454, true );
}

//Shortcode [fullwidth_latest_post]

function luckycasino_fullwidth_latest_post_shortcode_function( $atts ){
    extract( shortcode_atts( array(
        'button_text' => 'See Betting Tip',
        'category' => 'Uncategorized',
    ), $atts ) );

	ob_start();

	$params = array(
        'posts_per_page' => 1,
        'category_name'  => $category,
        'post_type'      => 'post',
        'orderby'        => 'date',
        'order'          => 'ASC',
    );

   $dateNow = date("Y-m-d H:i:s");

	$query = new WP_Query( array(
        'posts_per_page'   => 1,
        'category_name'    => $category,
        'post_type'        => 'post',
        'meta_key'         => 'start_date',
        'orderby'          => 'start_date',
        'order'            => 'ASC',
        //Returns upcoming matches >= today date
        'meta_query' => array(
            'relation' => 'OR',
                array(
                    'key'  => 'start_date',
                    'value' => $dateNow,
                    //'value' => '2021-08-02 18:00',
                    'type' => 'DATETIME',
                    'compare' => '>=',
                )
        ),
     ));

	if ($query->post_count < 1) {
        $query = new WP_Query( array_merge($params, array( 'meta_query' => array( array( 'key' => 'start_date', 'compare' => 'NOT EXISTS' ) ) ) ) );
		echo("Don't have events yet...");
    }

	    if ( $query->have_posts() ) { ?>
          <div class="fullwidth-latest-post">
            <?php
                $index = 0;
                while ( $query->have_posts() ) : $query->the_post();
                    $index++;
                    $thumbnail = get_the_post_thumbnail_url( get_the_ID(), 'fullwidth-latest-post-thumb' );
                    $post_link = get_page_link();
                    $isPostThumbnail = get_the_post_thumbnail();
                    $post_image = $isPostThumbnail ? $thumbnail : '';
                    $meta = get_post_meta(get_the_ID());

                    $home_team_logo_url = isset($meta['home_logo']) ? $meta['home_logo'][0] : '';
                    $away_team_logo_url = isset($meta['away_logo']) ? $meta['away_logo'][0] : '';
                    $start_date = isset($meta['start_date']) ? $meta['start_date'][0] : get_the_date();
                    $postDate = new DateTime($start_date);
                    $DD = $postDate->format('j M Y');
            ?>
             <div>
                 <figure>
                    <a href=<?php echo $post_link ?> class="figure-link">
                        <picture>
                            <div><img src=<?php echo $thumbnail; ?>></div>
                        </picture>
                        <?php if ( !empty($home_team_logo_url) && !empty($away_team_logo_url) )  { ?>
                            <div class="post_teams_logo">
                                <img width="50" height="50" src=<?php echo $home_team_logo_url; ?> alt="Home team logo" />
                                <img width="50" height="50" src=<?php echo $away_team_logo_url; ?> alt="Away team logo" />
                            </div>
                        <?php } ?>
                        <figcaption>
                            <h2><?php the_title(); ?></h2>
                            <div>
                                <time><?php echo $DD ?></time>
                                <span class="read-more"><?php echo $button_text ?> <i></i></span>
                            </div>
                        </figcaption>
                    </a>
                 </figure>
             </div>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </div>
    <?php }


	$content = ob_get_clean();
    return $content;
}


//Shortcode [latest_posts]
function luckycasino_latest_posts_shortcode_function( $atts ){
	extract( shortcode_atts( array(
        'post_to_show' => '4',
        'category' => 'Uncategorized',
    ), $atts ) );

     ob_start();

    $dateNow = date("Y-m-d H:i:s");

    $query = new WP_Query( array(
        'posts_per_page'   => $post_to_show,
        'category_name'    => $category,
        'post_type'        => 'post',
        'meta_key'         => 'start_date',
        'orderby'          => 'start_date',
        'order'            => 'ASC',
        'offset'           => 1,
        //Returns upcoming matches >= today date
        'meta_query' => array(
            'relation' => 'OR',
                array(
                    'key'  => 'start_date',
                    'value' => $dateNow,
                    'type' => 'DATETIME',
                    'compare' => '>=',
                )
        ),
     ) );

	if ($query->post_count < 1) {
		echo("Don't have events yet...");
    }

   if ( $query->have_posts() ) { ?>
         <div class="latest-post-list">
               <?php
                    $index = 0;
                    while ( $query->have_posts() ) : $query->the_post();
                        $index++;
                        $className = '';
                        $large_thumbnail_src = get_the_post_thumbnail_url( get_the_ID(), 'latest-large-post-thumb' );
                        $thumbnail_src = get_the_post_thumbnail_url( get_the_ID(), 'latest-post-thumb' );
                        $thumbnail = $large_thumbnail_src;
                        $post_link = get_permalink();
                        $isPostThumbnail = get_the_post_thumbnail();
                        $post_image = $isPostThumbnail ? $large_thumbnail_src : "";
                        $post_image_large = $isPostThumbnail ? $thumbnail : "";
                        $meta = get_post_meta(get_the_ID());
                        $home_team_logo_url = isset($meta['home_logo']) ? $meta['home_logo'][0] : '';
                        $away_team_logo_url = isset($meta['away_logo']) ? $meta['away_logo'][0] : '';

                        $post_id = get_the_ID();
                        $home_name  = get_post_meta($post_id, 'home_name', true);
                        $away_name  = get_post_meta($post_id, 'away_name', true);

                        //$team_1  = get_post_meta($post_id, 'team_1', true);
                        //$team_2  = get_post_meta($post_id, 'team_2', true);

                        $title = ($home_name && $away_name) ? '' . $home_name . ' vs ' . $away_name . '' : get_the_title();
               ?>
                <div class=<?php echo $className?>>
                    <figure>
						<a href=<?php echo $post_link ?> class="figure-link">
                        	<div><img src=<?php echo $thumbnail; ?>></div>
                       	 	<figcaption>
                           		<h2><a href=<?php echo $post_link ?>> <?php echo $title; ?> </a></h2>
 	                        </figcaption>
						</a>
                    </figure>
                </div>
               <?php endwhile;
               wp_reset_postdata(); ?>
       </div>

       <?php
   }
	$content = ob_get_clean();
       return $content;
}
